organization := "net.michalsitko"

name := "chess-problem"

version := "0.1"

scalaVersion := "2.11.1"

mainClass in Compile := Some("net.michalsitko.problem.Main")

libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.1" % Test

libraryDependencies += "com.github.scopt" %% "scopt" % "3.2.0"

resolvers += Resolver.sonatypeRepo("public")
