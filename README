# Chess Problem

## Usage

This is an sbt-backed project. You can see usage by typing `sbt run` in your console. The output of this command is also included below:

    Usage: chess-problem [options]
      -w <value> | --width <value>
            chessboard width
      -h <value> | --height <value>
            chessboard height
      -k <value> | --kings <value>
            number of kings, assumes 0 if not specified
      -q <value> | --queens <value>
            number of queens, assumes 0 if not specified
      -b <value> | --bishops <value>
            number of bishops, assumes 0 if not specified
      -r <value> | --rooks <value>
            number of rooks, assumes 0 if not specified
      -n <value> | --knights <value>
            number of knights, assumes 0 if not specified
      --no-output
            disable printing all solutions

### Example

To find number of solutions for 7x7 chessboard with 2 Kings, 2 Queen, 2 Bishops and 1 Knight type in your console:

    sbt "run -w 7 -h 7 -k 2 -q 2 -b 2 -n 1 --no-output"

or using sbt console:

    run -w 7 -h 7 -k 2 -q 2 -b 2 -n 1 --no-output

To find solutions for 7x7 chessboard with 2 Kings, 2 Queen, 2 Bishops and 1 Knight type in your console:

    sbt "run -w 7 -h 7 -k 2 -q 2 -b 2 -n 1"

or using sbt console:

    run -w 7 -h 7 -k 2 -q 2 -b 2 -n 1

## Tests

You can run tests with `sbt run`. There are tests covering crucial methods and integration tests in SolverTest class
which test behavior of whole project for given examples.