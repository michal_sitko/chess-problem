package net.michalsitko.problem.solver

import net.michalsitko.problem.{Config}
import net.michalsitko.problem.chess._

import scala.annotation.tailrec

/**
 * Created by michal on 24/10/14.
 */

case class Solver(config: Config){
  type PiecesLeft = List[Piece]

  /**
   * Returns stream of solutions.
   *
   * @return
   */
  def solve: Stream[Solution] = {
    val result = run(Stream(Perspective.initial(config)), config.piecesLeft)
    for(perspective <- result) yield Solution(perspective.appliedMoves)
  }

  @tailrec
  final private def run(prevPerspective: Stream[Perspective], piecesLeft: PiecesLeft): Stream[Perspective] = piecesLeft match {
    case h :: t => run(nextLevelPerspectives(prevPerspective, piecesLeft), t)
    case Nil => prevPerspective
  }

  private def nextLevelPerspectives(prevPerspectives: Stream[Perspective], piecesLeft: PiecesLeft): Stream[Perspective] = {
    def getNextMoves(perspective: Perspective, nextPiece: Piece): Stream[Move] = {
      val prevMove = perspective.appliedMoves.headOption
      val nextFields = prevMove match {
        case Some(m) if m.piece == nextPiece => m.field.nextFields
        case _ => Field(0, 0, config.dimensions) #:: Field(0, 0, config.dimensions).nextFields
      }
      nextFields.map(Move(nextPiece, _))
    }

    // we can safely get head because the only method calling nextLevelPerspectives is run and run checks if piecesLeft is nonempty
    val nextPiece = piecesLeft.head

    // for each previous Perspective we are generating next moves, applied them and filter out not defined perspectives
    prevPerspectives.flatMap(perspective => {
      val nextMoves: Stream[Move] = getNextMoves(perspective, nextPiece)
      val perspectivesAfterMove = nextMoves.map(nextMove => perspective.applyMove(nextMove))

      for{
        currentPerspective <- perspectivesAfterMove
        if currentPerspective.isDefined
      } yield currentPerspective.get
    })
  }

}
