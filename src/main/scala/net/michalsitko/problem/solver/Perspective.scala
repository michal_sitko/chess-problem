package net.michalsitko.problem.solver

import net.michalsitko.problem.{Config, ChessboardDimensions}
import net.michalsitko.problem.chess.{Field, Piece}

import scala.annotation.tailrec

/**
 * PiecePerspective represents available fields for concrete piece.
 */
case class PiecePerspective(availableFields: List[Field], perspectiveOwner: Piece, dimensions: ChessboardDimensions){
  /**
   * Returns new PiecePerspective which is result of applying given move.
   * Returns None if given move cannot be applied.
   *
   * @param move
   * @return
   */
  def applyMove(move: Move): Option[PiecePerspective] = {

    // List.contains can be used instead but this implementation takes advantage of the fact that elements in haystack are ordered
    @tailrec
    def contains(haystack: List[Field], needle: Field): Boolean = haystack match {
      case h :: t if h.id < needle.id => contains(t, needle)
      case h :: t if h.id == needle.id => true
      case h :: t if h.id > needle.id => false
      case Nil => false
    }

    if(contains(availableFields, move.field)){
      Some(update(move))
    }else{
      None
    }
  }

  /**
   * Returns new PiecePerspective which is result of applying given move.
   *
   * @param move
   * @return
   */
  def update(move: Move): PiecePerspective = {
    def notAttacked(field: Field): Boolean = {
      !move.piece.attacks(move.field, field) && !perspectiveOwner.attacks(move.field, field)
    }

    val newAvailableFields = availableFields.filter(notAttacked _)
    this.copy(availableFields = newAvailableFields)
  }
}

case class Perspective(perspectives: Map[Piece, PiecePerspective], appliedMoves: List[Move]){

  /**
   * Returns new Perspective which is result of applying given move.
   * Returns None if given move cannot be applied.
   *
   * @param move
   * @return
   */
  def applyMove(move: Move): Option[Perspective] = perspectives(move.piece).applyMove(move) match {
    case Some(perspective) =>
      val newPerspectives = updatedMapsForWeaker(move) + (move.piece -> perspective)
      Some(Perspective(newPerspectives, move :: appliedMoves))
    case None =>
      None
  }

  private def updatedMapsForWeaker(move: Move): Map[Piece, PiecePerspective] = {
    move.piece.getWeaker.foldLeft(perspectives)((acc, currentPiece) => {
      val activePerspective = perspectives.get(currentPiece)
      val accumulated = activePerspective.map(currentPerspective => acc + (currentPiece -> currentPerspective.update(move)))
      accumulated.getOrElse(acc)
    })
  }
}

object Perspective{
  /**
   * Returns initial Perspective for given config.
   *
   * @param config
   * @return
   */
  def initial(config: Config): Perspective = {
    val perspectives = initialPerspectives(config)

    // perspectives for pieces that are not included in request are not needed - filter them out
    val filteredPerspectives = perspectives.filterKeys(config.piecesLeft.contains(_))
    Perspective(filteredPerspectives, List[Move]())
  }

  private def initialPerspectives(config: Config): Map[Piece, PiecePerspective] = {
    Piece.byAge.foldLeft(Map[Piece, PiecePerspective]())((acc, piece) =>
      acc + (piece -> PiecePerspective(config.dimensions.allFields.toList, piece, config.dimensions))
    )
  }
}
