package net.michalsitko.problem.solver

import net.michalsitko.problem.ChessboardDimensions
import net.michalsitko.problem.chess.{Field, Piece}

/**
 * This class stores information about solution
 */
case class Solution(moves: List[Move]){
  def asString(chessboardDimensions: ChessboardDimensions): String = {
    def rowToString(fields: List[Field]): String = {
      val stringSeq = fields.map(currentField => {
        moves.find(currentField == _.field).map(_.piece.symbol).getOrElse(Field.EmptyFieldSymbol)
      })
      stringSeq.mkString("")
    }

    val rows: List[List[Field]] = chessboardDimensions.allFields.toList.grouped(chessboardDimensions.width).toList

    // the first line in list is supposed to be printed as last line
    val reversed = rows.reverse
    reversed.map(rowToString _).mkString(sys.props("line.separator"))
  }
}

object Solution{
  /**
   * Constructs solution from string. Useful only for test purposes - allows to type expected solutions declaratively.
   *
   * @param input
   * @return
   */
  def fromString(input: String): Solution = {
    val splitted = input.split(sys.props("line.separator"))
    val rows = splitted.size
    val columns = splitted.maxBy(_.size).size

    // the first line in the input string relates to the row with highest rowIndex - we need to reverse
    val reversed = splitted.reverse
    val moves = for (rowIndex <- 0 until rows;
                              columnIndex <- 0 until columns;
                              piece = Piece.fromSymbol(reversed(rowIndex)(columnIndex))
                              if piece.isDefined
    ) yield {
      Move(piece.get, Field(rowIndex, columnIndex, ChessboardDimensions(columns, rows)))
    }

    Solution(moves.toList)
  }
}

case class Move(piece: Piece, field: Field)
