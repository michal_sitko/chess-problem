package net.michalsitko.problem

import net.michalsitko.problem.chess._
import net.michalsitko.problem.solver._

/**
 * Main class.
 */
object Main {
  val parser = new scopt.OptionParser[Config]("chess-problem") {
    head("chess-problem", "0.1")
    opt[Int]('w', "width") required() action { (value, config) =>
      config.copy(width = value) } validate { value =>
      if(value > 0) success else failure("width must be positive") } text("chessboard width")

    opt[Int]('h', "height") required() action { (value, config) =>
      config.copy(height = value) } validate { value =>
      if(value > 0) success else failure("height must be positive") } text("chessboard height")
    opt[Int]('k', "kings") action { (value, config) =>
      config.copy(kingsNumber = value)} validate { value =>
      if(value >= 0) success else failure("number of kings must be non negative") } text("number of kings, assumes 0 if not specified")
    opt[Int]('q', "queens") action { (value, config) =>
      config.copy(queensNumber = value)} validate { value =>
      if(value >= 0) success else failure("number of queens must be non negative") } text("number of queens, assumes 0 if not specified")
    opt[Int]('b', "bishops") action { (value, config) =>
      config.copy(bishopsNumber = value)} validate { value =>
      if(value >= 0) success else failure("number of bishops must be non negative") } text("number of bishops, assumes 0 if not specified")
    opt[Int]('r', "rooks") action { (value, config) =>
      config.copy(rooksNumber = value)} validate { value =>
      if(value >= 0) success else failure("number of rooks must be non negative") } text("number of rooks, assumes 0 if not specified")
    opt[Int]('n', "knights") action { (value, config) =>
      config.copy(knightsNumber = value)} validate { value =>
      if(value >= 0) success else failure("number of knights must be non negative") } text("number of knights, assumes 0 if not specified")
    opt[Unit]("no-output") action { (_, config) =>
      {config.copy(outputEnabled = false) }} text("disable printing all solutions")
  }

  def main(args: Array[String]): Unit = {
    parser.parse(args, Config()).map{ config =>
      val solver = Solver(config)
      println("Solving...")

      // it is very important not to assign solver.solve to any variable here
      // assignment to variable would cause storing reference to head of stream which would prevent gc from releasing already used solutions
      // Stream of solutions should be consumed by tail recursive function (eg. foldLeft)
      val size = solver.solve.foldLeft(0)((acc, solution) => {
        if(config.outputEnabled){
          println(solution.asString(config.dimensions))
          println("")
        }
        acc + 1
      })
      println("unique configurations: " + size)
    }
  }
}

case class Config(width: Int = 0, height: Int = 0, kingsNumber: Int = 0, queensNumber: Int = 0, bishopsNumber: Int = 0, rooksNumber: Int = 0, knightsNumber: Int = 0, outputEnabled: Boolean = true){
  def piecesLeft: List[Piece] = {
    val configs = List((Queen, queensNumber),
      (Rook, rooksNumber),
      (Bishop, bishopsNumber),
      (Knight, knightsNumber),
      (King, kingsNumber))

    configs.flatMap(pieceNumberTuple => List.fill(pieceNumberTuple._2)(pieceNumberTuple._1))
  }

  lazy val dimensions: ChessboardDimensions = ChessboardDimensions(width, height)
}

case class ChessboardDimensions(width: Int, height: Int){
  lazy val allFields: Stream[Field] = {
    val list = for(row <- 0 until height; column <- 0 until width) yield Field(row, column, this)
    list.toStream
  }
}
