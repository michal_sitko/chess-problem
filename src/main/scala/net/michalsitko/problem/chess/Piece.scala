package net.michalsitko.problem.chess

/**
 * Trait represents Piece in the game of chessboard. All implementing classes must implements methods "symbol" and "attacks".
 */
sealed abstract trait Piece{
  /**
   * Returns symbol of piece. Useful for printing/reading chessboard.
   *
   * @return
   */
  def symbol: String

  /**
   * Returns true if Field(row, column) is attacked by piece staying in activeField.
   * Note that attacks(activeField, activeField.row, activeField.column) returns true.
   *
   * @param activeField
   * @param row
   * @param column
   * @return
   */
  def attacks(activeField: Field, row: Int, column: Int): Boolean

  /**
   * Returns true if fieldToTest is attacked by piece staying in activeField.
   * Note that attacks(activeField, activeField) returns true.
   *
   * @param activeField
   * @param fieldToTest
   * @return
   */
  def attacks(activeField: Field, fieldToTest: Field): Boolean = attacks(activeField, fieldToTest.row, fieldToTest.column)

  /**
   * Returns list of weaker pieces than piece referenced by this.
   */
  lazy val getWeaker: List[Piece] = Piece.byAge.dropWhile(_ != this).tail
}

object Piece {
  def fromSymbol(symbol: Char): Option[Piece] = symbol match {
    case 'k' => Some(King)
    case 'q' => Some(Queen)
    case 'b' => Some(Bishop)
    case 'n' => Some(Knight)
    case 'r' => Some(Rook)
    case _ => None
  }

  val byAge = List(Queen, Rook, Bishop, Knight, King)
}

object PieceHelper{
  def sameColumnRow(field: Field, anotherField: Field): Boolean = {
    field.column == anotherField.column || field.row == anotherField.row
  }

  def sameColumnRow(field: Field, row: Int, column: Int): Boolean = {
    field.column == column || field.row == row
  }

  def sameDiagonal(field: Field, anotherField: Field): Boolean = {
    val columnDiff = Math.abs(field.column - anotherField.column)
    val rowDiff = Math.abs(field.row - anotherField.row)
    columnDiff == rowDiff
  }

  def sameDiagonal(field: Field, row: Int, column: Int): Boolean = {
    val columnDiff = Math.abs(field.column - column)
    val rowDiff = Math.abs(field.row - row)
    columnDiff == rowDiff
  }
}

/********** Concrete implementations **********/

case object King extends Piece{
  override def symbol: String = "k"

  override def attacks(activeField: Field, row: Int, column: Int): Boolean = {
    val columnDiff = Math.abs(activeField.column - column)
    val rowDiff = Math.abs(activeField.row - row)
    columnDiff <= 1 && rowDiff <= 1
  }
}

object Queen extends Piece{
  override def symbol: String = "q"

  override def attacks(activeField: Field, row: Int, column: Int): Boolean = {
    PieceHelper.sameColumnRow(activeField, row, column) || PieceHelper.sameDiagonal(activeField, row, column)
  }
}

object Bishop extends Piece{
  override def symbol: String = "b"

  override def attacks(activeField: Field, row: Int, column: Int): Boolean = {
    PieceHelper.sameDiagonal(activeField, row, column)
  }
}

object Knight extends Piece{
  override def symbol: String = "n"

  override def attacks(activeField: Field, row: Int, column: Int): Boolean = {
    val columnDiff = Math.abs(activeField.column - column)
    val rowDiff = Math.abs(activeField.row - row)
    (columnDiff == 2 && rowDiff == 1) || (columnDiff == 1 && rowDiff == 2) || (columnDiff == 0 && rowDiff == 0)
  }
}

object Rook extends Piece{
  override def symbol: String = "r"

  override def attacks(activeField: Field, row: Int, column: Int): Boolean = {
    PieceHelper.sameColumnRow(activeField, row, column)
  }
}
