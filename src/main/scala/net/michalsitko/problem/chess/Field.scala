package net.michalsitko.problem.chess

import net.michalsitko.problem.{ChessboardDimensions}

/**
 * Created by michal on 05/10/14.
 */
case class Field(row: Int, column: Int, dimensions: ChessboardDimensions) {
  /**
   * Returns next fields. Result does not include field referenced by this object.
   *
   * @return
   */
  def nextFields: Stream[Field] = {
    dimensions.allFields.drop(id + 1)
  }

  /**
   * Useful for comparing Fields and algorithms based on random access.
   */
  lazy val id: Int = row * dimensions.width + column
}

object Field{
  val EmptyFieldSymbol = "."
}
