package net.michalsitko.problem

import net.michalsitko.problem.solver.Solution
import org.scalatest.FunSuite

/**
 * Created by michal on 02/10/14.
 */
class SolutionTest extends FunSuite {
  val exampleChessboardString =
    """|..kq
       |br..
       |..n.
    """.stripMargin.trim

  test("can import solution from string and export it back to string") {
    val solution = Solution.fromString(exampleChessboardString)
    assertResult(5)(solution.moves.size)
    assertResult(exampleChessboardString)(solution.asString(ChessboardDimensions(4, 3)))
  }
}
