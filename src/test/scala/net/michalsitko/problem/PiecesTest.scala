package net.michalsitko.problem

import net.michalsitko.problem.chess._
import org.scalatest.FunSuite

/**
 * Created by michal on 03/10/14.
 *
 * Tests method getAttackedBy of all pieces
 */
class PiecesTest extends FunSuite {
  val dimensions = ChessboardDimensions(7, 7)

  def newField(row: Int, column: Int) = Field(row, column, dimensions)

  test("King's attacks method"){
    val activeField = newField(1, 1)
    val fieldsExpectedToBeAttacked = List(newField(1,0), newField(1, 2), newField(0, 1), newField(0, 0), newField(2, 2))
    val fieldsNotExpectedToBeAttacked = List(newField(3, 0), newField(3, 2), newField(4, 4), newField(1, 6), newField(4, 1))

    fieldsExpectedToBeAttacked.foreach(field => assertResult(true)(King.attacks(activeField, field)))
    fieldsNotExpectedToBeAttacked.foreach(field => assertResult(false)(King.attacks(activeField, field)))
  }

  test("Queens's attacks method"){
    val activeField = newField(1, 1)
    val fieldsExpectedToBeAttacked = List(newField(1,0), newField(1, 2), newField(1, 6), newField(0, 1), newField(4, 1), newField(0, 0), newField(2, 2), newField(4, 4))
    val fieldsNotExpectedToBeAttacked = List(newField(3, 0), newField(3, 2))

    fieldsExpectedToBeAttacked.foreach(field => assertResult(true)(Queen.attacks(activeField, field)))
    fieldsNotExpectedToBeAttacked.foreach(field => assertResult(false)(Queen.attacks(activeField, field)))
  }

  test("Rook's attacks method"){
    val activeField = newField(1, 1)
    val fieldsExpectedToBeAttacked = List(newField(1,0), newField(1, 2), newField(1, 6), newField(0, 1), newField(4, 1))
    val fieldsNotExpectedToBeAttacked = List(newField(0, 0), newField(3, 2))

    fieldsExpectedToBeAttacked.foreach(field => assertResult(true)(Rook.attacks(activeField, field)))
    fieldsNotExpectedToBeAttacked.foreach(field => assertResult(false)(Rook.attacks(activeField, field)))
  }

  test("Bishop's attacks method"){
    val activeField = newField(1, 1)
    val fieldsExpectedToBeAttacked = List(newField(0,0), newField(2, 0), newField(4, 4), newField(0, 2))
    val fieldsNotExpectedToBeAttacked = List(newField(3, 2), newField(1, 0))

    fieldsExpectedToBeAttacked.foreach(field => assertResult(true)(Bishop.attacks(activeField, field)))
    fieldsNotExpectedToBeAttacked.foreach(field => assertResult(false)(Bishop.attacks(activeField, field)))
  }

  test("Knights's attacks method"){
    val activeField = newField(2, 2)
    val fieldsExpectedToBeAttacked = List(newField(3, 0), newField(4, 1), newField(4, 3), newField(3, 4), newField(1, 4), newField(0, 3), newField(0, 1), newField(1, 0))
    val fieldsNotExpectedToBeAttacked = List(newField(0, 0), newField(1, 1), newField(1, 2))

    fieldsExpectedToBeAttacked.foreach(field => assertResult(true)(Knight.attacks(activeField, field)))
    fieldsNotExpectedToBeAttacked.foreach(field => assertResult(false)(Knight.attacks(activeField, field)))
  }

  test("Field's nextFields method"){
    val dimensions = ChessboardDimensions(width = 3, height = 4)
    assertResult(11)(Field(0, 0, dimensions).nextFields.length)
    assertResult(10)(Field(0, 1, dimensions).nextFields.length)
    assertResult(8)(Field(1, 0, dimensions).nextFields.length)
    assertResult(0)(Field(3, 2, dimensions).nextFields.length)
  }
}
