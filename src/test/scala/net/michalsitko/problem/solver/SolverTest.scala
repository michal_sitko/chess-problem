package net.michalsitko.problem.solver

import org.scalatest.FunSuite

import net.michalsitko.problem.Config

/**
 * Created by michal on 02/10/14.
 * 
 * Tests Solver's solve method for given examples in integration manner
 */
class SolverTest extends FunSuite {
  test("Solver solves correctly for examples"){
    Example.allExamples.foreach{ example =>
      val solver = Solver(example.input)
      val solutions = solver.solve
      val actualOutputStrings = solutions.map(_.asString(example.input.dimensions))

      val expectedOutputStrings = example.output.map(_.asString(example.input.dimensions))
      assertResult(expectedOutputStrings.toSet)(actualOutputStrings.toList.toSet)
    }
  }
}

/**
 * Class for storing example input and its output.
 *
 */
case class Example(input: Config, output: List[Solution], height: Int, width: Int)
object Example{
  val input1 = Config(3, 3, 2, 0, 0, 1, 0)
  val output1 = List(
    """|k.k
      |...
      |.r.
    """.stripMargin.trim,
    """|k..
      |..r
      |k..
    """.stripMargin.trim,
    """|..k
      |r..
      |..k
    """.stripMargin.trim,
    """|.r.
      |...
      |k.k
    """.stripMargin.trim
  ).map(Solution.fromString(_))

  val input2 = Config(4, 4, 0, 0, 0, 2, 4)
  val output2 = List(
    """|.n.n
      |..r.
      |.n.n
      |r...
    """.stripMargin.trim,
    """|.n.n
      |r...
      |.n.n
      |..r.
    """.stripMargin.trim,
    """|r...
      |.n.n
      |..r.
      |.n.n
    """.stripMargin.trim,
    """|..r.
      |.n.n
      |r...
      |.n.n
    """.stripMargin.trim,
    """|.r..
      |n.n.
      |...r
      |n.n.
    """.stripMargin.trim,
    """|...r
      |n.n.
      |.r..
      |n.n.
    """.stripMargin.trim,
    """|n.n.
      |...r
      |n.n.
      |.r..
    """.stripMargin.trim,
    """|n.n.
      |.r..
      |n.n.
      |...r
    """.stripMargin.trim
  ).map(Solution.fromString(_))

  lazy val allExamples: List[Example] = List[Example](Example(input1, output1, 3, 3), Example(input2, output2, 4, 4))
}