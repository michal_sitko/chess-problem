package net.michalsitko.problem.solver

import net.michalsitko.problem.{ChessboardDimensions, Config}
import net.michalsitko.problem.chess.{Knight, Field, King, Bishop}
import org.scalatest.FunSuite

/**
 * Created by michal on 29/10/14.
 */
class PerspectiveTest extends FunSuite{
  test("Perspective's initial methods"){
    val config = Config(4, 4, kingsNumber = 2, bishopsNumber = 2)
    val perspective = Perspective.initial(config)

    assertResult(2)(perspective.perspectives.size)
    assert(perspective.perspectives.contains(King))
    assert(perspective.perspectives.contains(Bishop))

    perspective.perspectives.foreach{
      case (key, piecePerspective) => assertResult(16)(piecePerspective.availableFields.size)
    }

    perspective.perspectives.foreach{
      case (key, piecePerspective) => assertResult(16)(piecePerspective.availableFields.size)
    }
  }

  test("Perspective's applyMove method"){
    val config = Config(4, 4, kingsNumber = 2, bishopsNumber = 2, knightsNumber = 1)
    val perspective = Perspective.initial(config)

    val newPerspectiveOption = perspective.applyMove(Move(Bishop, Field(0, 1, config.dimensions)))

    assert(newPerspectiveOption.isDefined)
    val newPerspective = newPerspectiveOption.get
    assertResult(1)(newPerspective.appliedMoves.size)

    val attackedByKnight = List(Field(2, 0, config.dimensions), Field(2, 2, config.dimensions), Field(1, 3, config.dimensions))
    val attackedByBishop = List(Field(1, 0, config.dimensions), Field(1, 2, config.dimensions), Field(2, 3, config.dimensions), Field(0, 1, config.dimensions))
    val attackedByKing = List(Field(0, 0, config.dimensions), Field(1, 0, config.dimensions), Field(1, 1, config.dimensions), Field(1, 2, config.dimensions), Field(0, 2, config.dimensions))

    assertAvailableFields(newPerspective.perspectives(Bishop), attackedByBishop, config.dimensions)
    assertAvailableFields(newPerspective.perspectives(King), attackedByBishop ++ attackedByKing, config.dimensions)
    assertAvailableFields(newPerspective.perspectives(Knight), attackedByBishop ++ attackedByKnight, config.dimensions)
  }

  def assertAvailableFields(perspective: PiecePerspective, expectedToBeUnavailable: List[Field], dimensions: ChessboardDimensions) = {
    expectedToBeUnavailable.foreach(field => assertResult(false)(perspective.availableFields.contains(field)))

    val expectedToBeAvailable = dimensions.allFields.toList diff expectedToBeUnavailable
    expectedToBeAvailable.foreach(field => assert(perspective.availableFields.contains(field)))
  }
}
